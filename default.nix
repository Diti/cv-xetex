with import <nixpkgs> {}; let
  filenameBase = "cv-fr_dimitri-torterat";
  isoDate = "2023-02-05";

  pname = "diti-cv-xetex";
  version = builtins.replaceStrings ["-"] ["."] isoDate;

  fontConfig = makeFontsConf {
    fontDirectories = with pkgs; [
      atkinson-hyperlegible
      ibm-plex
      tex-gyre.termes
    ];
  };
in
  stdenvNoCC.mkDerivation {
    name = "${pname}-${version}";
    src = ./src;
    meta = with lib; {
      description = "The LaTex-generated CV of Dimitri Torterat";
      homepage = "https://gitlab.com/Diti/cv-xetex";
      license = licenses.eupl12;
      platforms = platforms.all;
    };

    buildInputs = with pkgs; [
      asciidoctor
      coreutils
      fontconfig
      (texlive.combine {
        inherit
          (texlive)
          mdwtools
          media9 # needed by ocgx2
          newunicodechar # needed by uspace
          ocgx2
          scheme-small
          titling
          uspace
          ;
      })
    ];

    preBuild = ''
      mkdir --parents .cache
      chmod --recursive u+w .cache
      export FONTCONFIG_FILE=${fontConfig}
      export SOURCE_DATE_EPOCH=$(date --date='${isoDate}' +%s)
      export XDG_CACHE_HOME=.cache
    '';
    buildPhase = ''
      runHook preBuild
      xelatex -interaction=nonstopmode ${filenameBase}.ltx
      xelatex -interaction=nonstopmode ${filenameBase}.ltx
      runHook postBuild
    '';
    installPhase = ''
      runHook preInstall
      mkdir --parents $out
      cp ${filenameBase}.pdf $out/${filenameBase}_${isoDate}.pdf
      runHook postInstall
    '';
  }
